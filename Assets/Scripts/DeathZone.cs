﻿using UnityEngine;
using System.Collections;

public class DeathZone : MonoBehaviour {
	public Player player;

	// Update is called once per frame
	void Update () {
		//Reloads level if Players position is below this gameobject
		if (player.transform.position.y < this.transform.position.y) {
			Application.LoadLevel(Application.loadedLevelName);
		}
	}
}
