﻿using UnityEngine;
using System.Collections;

public class SnailEnemy : MonoBehaviour {
	public Vector2 velocity; //Enemy velocity

	//Enemy sight
	public Transform sightStart;
	public Transform sightEnd;
	public Transform groundSight;

	//What enemy actually detects
	public LayerMask sightMask;
	public LayerMask groundMask;
	

	public AudioClip snailSound;
	private AudioSource audioSource;
	private Rigidbody2D body;
	private Animator animator;

	private bool colliding;	//Enemy sight flag
	private bool isOnGround; //Enemy on ground
	private int life; //Enemys life
	private float inShell; //Duration in shell
	private bool alive = true;

	// Use this for initialization
	void Start () {
		audioSource = GetComponent<AudioSource> ();
		body = GetComponent<Rigidbody2D> ();
		animator = GetComponent<Animator> ();
		life = 1;
		inShell = 0;
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		//Movement
		if (alive && life == 1) {
			body.velocity = velocity;

		}
	}
	
	void Update(){
		colliding = Physics2D.Linecast (sightStart.position, sightEnd.position, sightMask); //Checks if enemy has detected anything
		isOnGround = Physics2D.OverlapCircle(groundSight.position, 0.2f, groundMask);  //Checks if enemy is on ground

		//Turn around if anything detected or not on ground
		if (colliding || !isOnGround) {
			transform.localScale = new Vector3(transform.localScale.x * -1, transform.localScale.y, transform.localScale.z);
			velocity *= -1;
			
		}
		
		//WakeUp after x seconds
		if (Time.realtimeSinceStartup - inShell >= 2f && animator.GetBool("shell")) {
			WakeUp();
			inShell = Time.realtimeSinceStartup;
		}
	}

	//Stomped
	void OnTriggerEnter2D(Collider2D obj){
		//Checks if Player has stomped enemy and still has one life
		if (obj.tag == "Player" && alive && !Player.knockbacked && life == 1) {
			life--; //Lost life
			ToShell(); //Go to shell
			obj.attachedRigidbody.AddForce(new Vector2(0f,400f)); //Add force to player
		}else if(obj.tag == "Player" && alive && !Player.knockbacked && life == 0){
			Die();
			obj.attachedRigidbody.AddForce(new Vector2(0f,400f));
		}
	}

	//Debug Draw
	void OnDrawGizmos(){
		Gizmos.color = Color.red;
		
		Gizmos.DrawLine (sightStart.position, sightEnd.position);
		Gizmos.DrawSphere (groundSight.position, 0.05f);
	}

	//Go to Shell
	public void ToShell(){
		audioSource.clip = snailSound;
		audioSource.Play();
		body.velocity = new Vector2 (0, 0);
		animator.SetBool ("shell", true);
		inShell = Time.realtimeSinceStartup;
	}

	//Come out of Shell
	public void WakeUp(){
		animator.SetBool ("wake", true);
		animator.SetBool ("shell", false);
		life++;
		inShell = Time.realtimeSinceStartup;
	}

	//Dead
	public void Die(){
		audioSource.clip = snailSound;
		audioSource.Play();
		body.velocity = new Vector2 (0, 0); //Disable movement
		animator.SetBool ("dead", true); //Play animation
		alive = false; //Changing tag to stomped

		//Disable all colliders
		foreach(Collider2D c in GetComponents<Collider2D> ()) {
			c.enabled = false;
		}
		body.isKinematic = true;
		Destroy (this.gameObject, 2f); //Wait 2s before destroying object
	}
}
