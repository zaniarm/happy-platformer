﻿using UnityEngine;
using System.Collections;

public class Spider: MonoBehaviour {
	public Vector2 velocity; //Enemy velocity
	
	//Enemy sight
	public Transform sightStart;
	public Transform sightEnd;
	
	//Checks if ground is under enemy
	public Transform groundSight;
	
	//Determines what enemy can actually detect
	public LayerMask sightMask;
	public LayerMask groundMask;
	
	public AudioClip spiderSound;
	private AudioSource audioSource;
	private Animator animator;
	private Rigidbody2D body;

	public Transform player; //reference to players transform

	public float minDistance;
	private float range;
	private bool isOnGround, colliding;
	private bool facingRight = false;
	private bool alive = true;

	// Use this for initialization
	void Start () {
		audioSource = GetComponent<AudioSource> ();
		body = GetComponent<Rigidbody2D> ();
		animator = GetComponent<Animator> ();
	}
	
	void Update(){
		colliding = Physics2D.Linecast (sightStart.position, sightEnd.position, sightMask);  //Checks if Enemy detects anything
		isOnGround = Physics2D.OverlapCircle(groundSight.position, 0.2f, groundMask);	//Checks if enemy is on ground

		//move towards the player
		range = Vector2.Distance(transform.position, player.position);

		//Turn around
		if (colliding || !isOnGround) {
			transform.localScale = new Vector3 (transform.localScale.x * -1, transform.localScale.y, transform.localScale.z);
			velocity *= -1;
		}

		if ((player.position.x - transform.position.x) > 0 && range < minDistance && !colliding && !facingRight) {
			transform.localScale = new Vector3 (transform.localScale.x * -1, transform.localScale.y, transform.localScale.z);
			facingRight = true;
		} else if ((player.position.x - transform.position.x) < 0 && range < minDistance && !colliding && facingRight) {
			transform.localScale = new Vector3 (transform.localScale.x * -1, transform.localScale.y, transform.localScale.z);
			facingRight = false;
		}

		//Move towards player
		if (range < minDistance && !colliding && alive) {
			animator.SetBool ("move", true);

			//Play Sound
			if(!audioSource.isPlaying){
				audioSource.clip = spiderSound;
				audioSource.Play();
			}

			//Movement
			transform.position = Vector2.MoveTowards (transform.position, player.position, velocity.x * Time.deltaTime);
		} else {
			animator.SetBool ("move", false);
		}
	}
	
	
	void OnTriggerEnter2D(Collider2D obj){
		//If player stomps enemy
		if (obj.tag == "Player" && !Player.knockbacked && alive) {
			Die();
			obj.attachedRigidbody.AddForce(new Vector2(0f,400f), ForceMode2D.Force); //Add knockback to player
		}
	}
	
	//Debug draw
	void OnDrawGizmos(){
		Gizmos.color = Color.red;
		
		Gizmos.DrawLine (sightStart.position, sightEnd.position);
		Gizmos.DrawSphere (groundSight.position, 0.05f);
	}
	
	//Enemy died
	public void Die(){
		//Play sound
		audioSource.clip = spiderSound;
		audioSource.Play ();
		body.velocity = new Vector2 (0, 0); //Disable movement
		animator.SetBool ("stomped", true); //Play animation
		animator.SetBool ("move", false);
		alive = false; //Enemy is stomped
		
		//Remove all colliders
		foreach(Collider2D c in GetComponents<Collider2D> ()) {
			c.enabled = false;
		}
		
		body.isKinematic = true; //Disable physics
		Destroy (this.gameObject, 2f);
	}
}
