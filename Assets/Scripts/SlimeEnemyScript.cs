﻿using UnityEngine;
using System.Collections;

public class SlimeEnemyScript : MonoBehaviour {
	public Vector2 velocity;

	//Enemys sight
	public Transform sightStart;
	public Transform sightEnd;

	//Checks if ground is under enemy
	public Transform groundSight;

	//Determines what enemy can actually detect
	public LayerMask sightMask;
	public LayerMask groundMask;
	
	private AudioSource audioSource;
	private bool alive = true;
	
	public AudioClip slimeSound;
	private Rigidbody2D body;
	private bool colliding;	//Checks if player has detected anything
	private bool isOnGround; //Checks if player is on ground

	//Timer variables for movement
	private float lastMovement;
	private bool move;  //Checks if enemy is allowed to move


	// Use this for initialization
	void Start () {
		audioSource = GetComponent<AudioSource> ();
		body = GetComponent<Rigidbody2D> ();
		lastMovement = 0;
		move = false;
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		if (move && alive && isOnGround) {
			body.AddForce (new Vector2(velocity.x, velocity.y), ForceMode2D.Impulse);
			Debug.Log("Force");

			//If any audio is playing; Play slimesound 
			if(!audioSource.isPlaying){
				audioSource.clip = slimeSound;
				audioSource.Play();
			}
		}
	}


	void Update(){
		colliding = Physics2D.Linecast (sightStart.position, sightEnd.position, sightMask);  //Checks if Enemy detects anything
		isOnGround = Physics2D.OverlapCircle(groundSight.position, 0.2f, groundMask);	//Checks if enemy is on ground

		//If enemy detects anythin in front of him or it isn't on ground then turn around
		if (colliding || !isOnGround) {
			transform.localScale = new Vector3(transform.localScale.x * -1, transform.localScale.y, transform.localScale.z);
			velocity *= -1;
			
		}

		//Move every three seconds
		if (Time.realtimeSinceStartup - lastMovement >= 3 && !move) {
			move = true;
			lastMovement = Time.realtimeSinceStartup;
		}

		//Move for .5 seconds
		if (Time.realtimeSinceStartup - lastMovement >= 0.5f && move) {
			move = false;
			lastMovement = Time.realtimeSinceStartup;
		}
	}


	void OnTriggerEnter2D(Collider2D obj){
		//Checks if Player has entered its trigger and enemy is not stopmed already
		if (obj.tag == "Player" && !Player.knockbacked && alive) {
			Die();
			obj.attachedRigidbody.AddForce(new Vector2(0f,300f));  //Add some force to player
		}
	}

	//Debug drawing
	void OnDrawGizmos(){
		Gizmos.color = Color.red;
		
		Gizmos.DrawLine (sightStart.position, sightEnd.position);
		Gizmos.DrawSphere (groundSight.position, 0.05f);
	}

	//Enemy dies
	void Die(){
		audioSource.clip = slimeSound; //Play sound
		audioSource.Play();
		body.velocity = new Vector2 (0, 0); //Disabling movement
		alive = false; //Changing tag

		//Removing Colliders
		foreach(Collider2D c in GetComponents<Collider2D> ()) {
			c.enabled = false;
		}

		body.isKinematic = true; //Disabling enemy physics
		Destroy (this.gameObject, 2f); //Destroy gameobject after 2s
	}
}
