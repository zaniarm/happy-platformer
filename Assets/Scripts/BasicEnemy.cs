﻿using UnityEngine;
using System.Collections;

public class BasicEnemy : MonoBehaviour {
	public Vector2 velocity; //Enemy velocity

	//Enemy sight
	public Transform sightStart;
	public Transform sightEnd;
	public Transform groundSight;

	//Determines what enemy actually detects
	public LayerMask sightMask;
	public LayerMask groundMask;

	public AudioClip stompedSound, moveSound;
	private AudioSource audiosource;
	private Animator animator;
	private Rigidbody2D body;
	private bool colliding;
	private bool isOnGround;
	private bool alive = true;


	// Use this for initialization
	void Start () {
		audiosource = GetComponent<AudioSource> ();
		body = GetComponent<Rigidbody2D> ();
		animator = GetComponent<Animator> ();
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		//Movement
		if (alive) {
			body.velocity = velocity;
		}
	}

	void Update(){
		colliding = Physics2D.Linecast (sightStart.position, sightEnd.position, sightMask); //Checks if anything is detected
		isOnGround = Physics2D.OverlapCircle(groundSight.position, 0.2f, groundMask); //Checks if enemy is on ground

		//Turn around if anyting detected or not on ground
		if (colliding || !isOnGround) {
			transform.localScale = new Vector3 (transform.localScale.x * -1, transform.localScale.y, transform.localScale.z);
			velocity *= -1;
		}

		if (!audiosource.isPlaying && moveSound != null) {
			audiosource.clip = moveSound;
			audiosource.Play ();
		}
	}


	void OnTriggerEnter2D(Collider2D obj){
		//If player stomps enemy
		if (obj.tag == "Player" && !Player.knockbacked && alive) {
			Die();
			obj.attachedRigidbody.AddForce(new Vector2(0f,400f), ForceMode2D.Force); //Add knockback to player
		}
	}

	//Debug draw
	void OnDrawGizmos(){
		Gizmos.color = Color.red;

		Gizmos.DrawLine (sightStart.position, sightEnd.position);
		Gizmos.DrawSphere (groundSight.position, 0.05f);
	}

	//Enemy died
	public void Die(){
		//Play sound
		audiosource.clip = stompedSound;
		audiosource.Play ();
		body.velocity = new Vector2 (0, 0); //Disable movement
		animator.SetBool ("stomped", true); //Play animation
		alive = false; //Enemy is stomped

		//Remove all colliders
		foreach(Collider2D c in GetComponents<Collider2D> ()) {
			c.enabled = false;
		}

		body.isKinematic = true; //Disable physics
		Destroy (this.gameObject, 2f);
	}
}