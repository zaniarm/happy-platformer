﻿using UnityEngine;
using System.Collections;

public class Player : MonoBehaviour {
	private float speed = 3;	//Players speed
	public float jumpForce;	//Players jumpForce
	private bool isOnGround;	//Player touches ground
	public Transform groundCheck;	//Reference for groundChecks transform
	public LayerMask groundMask;	//Mask of ground
	private AudioSource audioSource;	//Players Audiosource
	public AudioClip jumpSound, hitSound, keySound, coinSound;	//Sounds that Players audiosource plays

	private Vector2 velocity;	//Player velocity vector

	private Animator anim;
	private Rigidbody2D body;
	private UI ui;	//Reference to UI for handling ui elements

	private bool facingRight = true;	//Checks if player is facing right
	private bool jumping;	//Checks if Player is jumping

	private bool hasBlueKey, hasYellowKey, hasRedKey;
	private bool leftPressed, rightPressed, jumpPressed;


	public static bool knockbacked = false;	//Checks if player is currently being knockedback

	//Knockback forces on x and y axis
	public float knockbackForceX;
	public float knockbackForceY;

	//Handles duration of knockback
	public float knockbackTime;
	private float knockbackTimer = 0f;
	
	public int life = 3;	//Players life
	private float coins = 0;	//Collected coins

	// Use this for initialization
	void Start () {
		audioSource = GetComponent<AudioSource> ();
		anim = GetComponent<Animator>();
		body = GetComponent<Rigidbody2D>();
		ui = GetComponent<UI> ();
		knockbacked = false;
	}

	void FixedUpdate () {
		body.velocity = velocity;	//Setting velocity to rigidbody

		isOnGround = Physics2D.OverlapCircle(groundCheck.position, 0.2f, groundMask); //Checking if Player touches ground

		//Allowing jump when player touches ground and jump allowed
		if (isOnGround && jumping) {
			Jump ();
			jumping = false;
		}

	}

	// Update is called once per frame
	void Update(){
		jumping = false;

		//Checks knockback
		if (knockbacked && knockbackTimer < knockbackTime) {
			Knockback ();
		} else {
			knockbacked = false;
			knockbackTimer = 0f; //Resetting timer
		}

		//Debug Controls (Android buttons implemented with ui elements)
		if((Input.GetKey (KeyCode.RightArrow) || rightPressed) && !knockbacked){
			Walk ();
			velocity = new Vector2(speed, body.velocity.y);
		} else if ((Input.GetKey (KeyCode.LeftArrow) || leftPressed) && !knockbacked){
			Walk ();
			velocity = new Vector2(-speed, body.velocity.y);
		}else{
			velocity = new Vector2(0 , body.velocity.y);
		}

		//Jumping
		if ((Input.GetKey (KeyCode.UpArrow) || jumpPressed) && !knockbacked) {
			jumping = true;	//Currently jumping

			//Plays jump sound only once
			if(isOnGround){
				isOnGround = false; //Making sure that player is not on ground
				PlaySound(jumpSound);
			}
		}

		if (coins >= 10) {

			if(life < 3){
				life++;
				CheckHealth();
			}

			coins = 0;
			ui.UpdateCoins (coins.ToString());
		}
	}

	//Collision Detection
	void OnCollisionEnter2D(Collision2D obj){
		//Checks if player collides with enemy and player is not currently being knockedback
		if (obj.gameObject.tag == "Enemy" && !knockbacked) {
			knockbacked = true;
			life--;	//Lost life
			PlaySound(hitSound);
			CheckHealth();	//Update ui
		}
	}

	//Trigger Detection
	void OnTriggerEnter2D(Collider2D obj){

		//Check if Player enters Coin Trigger
		if (obj.gameObject.tag == "Coin") {
			PlaySound(coinSound);
			Destroy(obj.gameObject); //Destroy coin object
			coins++; //Coin collected
			ui.UpdateCoins(coins.ToString()); //Update ui
		}

		if (obj.gameObject.tag == "CoinBox") {
			PlaySound(coinSound);
			coins++;
			obj.tag = "Collected";
			obj.GetComponent<Animator>().SetBool("collected", true); //Play CoinBox's animation
			ui.UpdateCoins(coins.ToString());
		}
		
		if (obj.gameObject.tag == "BlueKey") {
			hasBlueKey = true;	//Key collected
			ui.SetBlueKeyUI();	//Update ui
			BlueFlag.PlayFlagAnimation();	//Play flag animation
			PlaySound(keySound);
			Destroy(obj.gameObject); //Destroy key object
		}
		
		if (obj.gameObject.tag == "YellowKey") {
			hasYellowKey = true;
			ui.SetYellowKeyUI();
			YellowFlag.PlayFlagAnimation();
			PlaySound(keySound);
			Destroy(obj.gameObject);
		}
		
		if (obj.gameObject.tag == "RedKey") {
			hasRedKey = true;
			ui.SetRedKeyUI();
			RedFlag.PlayFlagAnimation();
			PlaySound(keySound);
			Destroy(obj.gameObject);
		}

		//Checks if all keys have been collected
		if (hasRedKey && hasYellowKey && hasBlueKey) {
			Door.PlayDoorAnimation();
		}
	}
	
	void Knockback(){
		//Checks where Player is facing before adding force to body
		if (facingRight) {
			body.AddForce (new Vector2(-knockbackForceX,knockbackForceY) * 5f);
		} else {
			body.AddForce (new Vector2(knockbackForceX,knockbackForceY)* 2f);
		}

		knockbackTimer += Time.deltaTime; //Update Timer
	}

	void Jump(){
		Vector2 vel = body.velocity; //Getting bodys current velocity
		vel.y = jumpForce; //Setting jumpForce on vectors y-axis
		body.velocity = vel;	//setting velocity to body
	}

	//Checks if Players sprite needs to be flipped
	void Flip() {
		facingRight = !facingRight;
		Vector3 scale = transform.localScale; //Getting players current scale
		scale.x *= -1; //inverting players scale on x-axis
		transform.localScale = scale; //setting the new scale to player
	}

	//Play Animations
	void Turn(){
		anim.Play ("Turn");
	}
	
	void Idle(){
		anim.Play("Idle");
		velocity = new Vector2(0 , body.velocity.y); //stopping movement on x-axis
	}
	
	void Walk(){
		anim.Play ("Walk");
		
		if (facingRight && body.velocity.x < 0) {
			Turn();
			Flip ();
		} else if (!facingRight && body.velocity.x > 0) {
			Turn ();
			Flip ();
		}
	}

	//Checks if Player is still alive and updates ui
	void CheckHealth(){
		if(life == 3){
			ui.SetHeart3UI(false);
			ui.SetHeart2UI(false);
			ui.SetHeart1UI(false);
		} else if (life == 2) {
			ui.SetHeart3UI(true);
			ui.SetHeart2UI(false);
			ui.SetHeart1UI(false);
		} else if (life == 1) {
			ui.SetHeart3UI(true);
			ui.SetHeart2UI(true);
			ui.SetHeart1UI(false);
		} else { //If player is dead
			ui.SetHeart3UI(true);
			ui.SetHeart2UI(true);
			ui.SetHeart1UI(true);
			Application.LoadLevel(Application.loadedLevelName); //Load current level
		}
	}

	//Plays audioclips
	public void PlaySound(AudioClip clip){
			audioSource.clip = clip;
			audioSource.Play ();
	}


	//Methods for handling android button inputs
	public void JumpPressed(){
		jumpPressed = true;
	}

	public void LeftPressed(){
		leftPressed = true;
	}

	public void RightPressed(){
		rightPressed = true;
	}

	public void Stop(){
		rightPressed = false;
		leftPressed = false;
	}

	public void StopJumping(){
		jumpPressed = false;
	}
}