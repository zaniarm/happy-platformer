﻿using UnityEngine;
using System.Collections;

public class Door : MonoBehaviour {
	static Animator animator;

	// Use this for initialization
	void Start () {
		animator = GetComponent<Animator> ();
		Debug.Log (Application.loadedLevel + "," + Application.levelCount);
	}

	public static void PlayDoorAnimation(){
		animator.SetBool ("open", true);
	}

	//Trigger Handling
	void OnTriggerEnter2D(Collider2D obj){
		//Checks if player is colliding to opened door and that next level exists
		if (obj.tag == "Player" && animator.GetBool ("open") && Application.loadedLevel < Application.levelCount - 1) {
			Application.LoadLevel (Application.loadedLevel + 1);
			Debug.Log (Application.loadedLevel + " hei " + Application.levelCount);
		} else if (obj.tag == "Player" && animator.GetBool ("open") && Application.loadedLevel == Application.levelCount - 1) {
			Application.LoadLevel (0);
		}
	}
}
