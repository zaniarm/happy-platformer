﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class UI : MonoBehaviour {
	//UI elements
	public Image heart1, heart2, heart3;
	public Image redKey, yellowKey, blueKey;
	public Text text;

	//Variable for handling all ui animations
	public Animator animator;

	//Keys collected
	public void SetRedKeyUI(){
		animator = redKey.GetComponent<Animator> ();
		animator.SetBool ("aquired", true);
	}

	public void SetBlueKeyUI(){
		animator = blueKey.GetComponent<Animator> ();
		animator.SetBool ("aquired", true);
	}

	public void SetYellowKeyUI(){
		animator = yellowKey.GetComponent<Animator> ();
		animator.SetBool ("aquired", true);
	}

	//Life animations
	public void SetHeart1UI(bool lost){
		animator = heart1.GetComponent<Animator> ();
		animator.SetBool ("lost", lost);
	}

	public void SetHeart2UI(bool lost){
		animator = heart2.GetComponent<Animator> ();
		animator.SetBool ("lost", lost);
	}

	public void SetHeart3UI(bool lost){
		animator = heart3.GetComponent<Animator> ();
		animator.SetBool ("lost", lost);
	}

	//Coins collected
	public void UpdateCoins(string coins){
		text.text = coins;
	}
}