﻿using UnityEngine;
using System.Collections;

public class RedFlag : MonoBehaviour {
	static Animator animator;
	
	// Use this for initialization
	void Start () {
		animator = GetComponent<Animator> ();
	}
	
	public static void PlayFlagAnimation(){
		animator.SetBool ("KeyAquired", true);
	}
}
